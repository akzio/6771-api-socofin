# Contenido

En este repositorio se versionarán los archivos asociados al dominio de negocio Servicios -> Pagos --> RPA

Debe contener:
1. Descriptor del producto
2. Descriptor de las apis asociadas al producto
3. Descriptor con las organizaciones/aplicaciones suscritas a los planes de este producto.

Producto:      servicios-pagos-rpa-prd_1.0.0.yaml

API:           API.000150.PagosRPA.yaml

Suscripciones: servicios-pagos-rpa-prd_1.0.0-subscriptions.yaml
